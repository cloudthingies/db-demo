# db-demo

demo dynamodb for AWS deployed using serverless.

## customer-demo-db
For keeping customer information.
Indexed on unique CustomerID.

## account-demo-db
For keeping account information. Seperate DB since customer-account relationship is one to many.
Indexed on CustomerID and sorted on AccountID.

## transactions-demo-db
Used as a ledger. I believe other services as AWS QLDB could be better here, but DynamoDB is free.
Using a dynamoDB stream to an SQS and then writing to account-demo-db with transactions, idempotent operations can be achieved.
Indexed on unique TransactionID.

![architecture](./db-demo.drawio.png)